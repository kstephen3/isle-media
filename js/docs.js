function middle_menu(item){
	
    if(item=="notes"){
    	document.getElementById(""+item).style.background="#FF7F00";
    	document.getElementById(""+item).style.color="#ffffff";

    	document.getElementById("papers").style.background="#ffffff";
    	document.getElementById("papers").style.color="#000";

    	document.getElementById("coursework").style.background="#ffffff";
    	document.getElementById("coursework").style.color="#000";

    }else  if(item=="papers"){
    	document.getElementById(""+item).style.background="#FF7F00";
    	document.getElementById(""+item).style.color="#ffffff";

    	document.getElementById("notes").style.background="#ffffff";
    	document.getElementById("notes").style.color="#000";

    	document.getElementById("coursework").style.background="#ffffff";
    	document.getElementById("coursework").style.color="#000";
    }else  if(item=="coursework"){
    	document.getElementById(""+item).style.background="#FF7F00";
    	document.getElementById(""+item).style.color="#ffffff";

    	document.getElementById("notes").style.background="#ffffff";
    	document.getElementById("notes").style.color="#000";

    	document.getElementById("papers").style.background="#ffffff";
    	document.getElementById("papers").style.color="#000";
    }

    var middle =document.getElementById("middle_menu").value=item;

    if(middle=="coursework"){
        document.getElementById("deadline").innerHTML='<div class="row left1_top15">'+
            '<div class="col-lg-2 col-xl-2 col-m-2"><label class="top3 pull-right ">Deadline</label></div>'+
            '<div class="col-lg-8 col-xl-8 col-m-8">'+
                '<input class="form-control text_field_height toppad3" type="text" >'+
            '</div>'+
        '</div>';
    }else if(middle=="notes"){
        document.getElementById("deadline").innerHTML="";
    }else if(middle=="papers"){
        document.getElementById("deadline").innerHTML="";
    }



}

function side_menu_docs(item){
    
	if(item=="doc_upload"){
    	document.getElementById(""+item).style.background="#FF7F00";
    	document.getElementById(""+item).style.color="#ffffff";

    	document.getElementById("doc_myupload").style.background="#ffffff";
    	document.getElementById("doc_myupload").style.color="#000";

    	document.getElementById("doc_coursework").style.background="#ffffff";
    	document.getElementById("doc_coursework").style.color="#000";

    	document.getElementById("doc_notes").style.background="#ffffff";
    	document.getElementById("doc_notes").style.color="#000";

    	document.getElementById("doc_past_papers").style.background="#ffffff";
    	document.getElementById("doc_past_papers").style.color="#000";

    }else  if(item=="doc_myupload"){

    	document.getElementById(""+item).style.background="#FF7F00";
    	document.getElementById(""+item).style.color="#ffffff";

    	document.getElementById("doc_upload").style.background="#ffffff";
    	document.getElementById("doc_upload").style.color="#000";

    	document.getElementById("doc_coursework").style.background="#ffffff";
    	document.getElementById("doc_coursework").style.color="#000";

    	document.getElementById("doc_notes").style.background="#ffffff";
    	document.getElementById("doc_notes").style.color="#000";

    	document.getElementById("doc_past_papers").style.background="#ffffff";
    	document.getElementById("doc_past_papers").style.color="#000";

    }else  if(item=="doc_coursework"){

    	document.getElementById(""+item).style.background="#FF7F00";
    	document.getElementById(""+item).style.color="#ffffff";

    	document.getElementById("doc_upload").style.background="#ffffff";
    	document.getElementById("doc_upload").style.color="#000";

    	document.getElementById("doc_myupload").style.background="#ffffff";
    	document.getElementById("doc_myupload").style.color="#000";

    	document.getElementById("doc_notes").style.background="#ffffff";
    	document.getElementById("doc_notes").style.color="#000";

    	document.getElementById("doc_past_papers").style.background="#ffffff";
    	document.getElementById("doc_past_papers").style.color="#000";
    	
    }else  if(item=="doc_notes"){

    	document.getElementById(""+item).style.background="#FF7F00";
    	document.getElementById(""+item).style.color="#ffffff";

    	document.getElementById("doc_upload").style.background="#ffffff";
    	document.getElementById("doc_upload").style.color="#000";

    	document.getElementById("doc_myupload").style.background="#ffffff";
    	document.getElementById("doc_myupload").style.color="#000";

    	document.getElementById("doc_coursework").style.background="#ffffff";
    	document.getElementById("doc_coursework").style.color="#000";

    	document.getElementById("doc_past_papers").style.background="#ffffff";
    	document.getElementById("doc_past_papers").style.color="#000";
    	
    }else  if(item=="doc_past_papers"){

    	document.getElementById(""+item).style.background="#FF7F00";
    	document.getElementById(""+item).style.color="#ffffff";

    	document.getElementById("doc_upload").style.background="#ffffff";
    	document.getElementById("doc_upload").style.color="#000";

    	document.getElementById("doc_myupload").style.background="#ffffff";
    	document.getElementById("doc_myupload").style.color="#000";

    	document.getElementById("doc_coursework").style.background="#ffffff";
    	document.getElementById("doc_coursework").style.color="#000";

    	document.getElementById("doc_notes").style.background="#ffffff";
    	document.getElementById("doc_notes").style.color="#000";
    	
    }

    document.getElementById("side_menu_docs").value=item;

    // document.getElementById("loader").innerHTML="loading....";
    document.getElementById("loader").innerHTML='<img src="../../uploads/ajax-loader.gif">';
    

    $.get("Home_controller.php?tab="+item,function(data){
        document.getElementById('middle_page').innerHTML=data;
        $("#loader").css("display","none");
    });

}

function Home_controller(){

    $.get("Home_controller.php",function(data){
        document.getElementById('middle_page').innerHTML=data;
    });

}

function getCountries(){
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "http://filedocs-001-site1.ftempurl.com/api/api/FileDocsUser/CourseUnit/3",
      // "url": "http://localhost:37190/api/Country/GetCountries",
      "method": "GET",
      "headers": {},
      "processData": false,
      "data": "{}"
    }

    $.ajax(settings).done(function (response) {
      console.log(response);
      // var data =JSON.parse(response);
      // alert(response.Data[0].Cities[0].Universities[2].CampusName);

    });
}


jssor_1_slider_init = function() {
            
    var jssor_1_SlideshowTransitions = [
      {$Duration:1200,x:0.2,y:-0.1,$Delay:20,$Cols:8,$Rows:4,$Clip:15,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Assembly:260,$Easing:{$Left:$Jease$.$InWave,$Top:$Jease$.$InWave,$Clip:$Jease$.$OutQuad},$Outside:true,$Round:{$Left:1.3,$Top:2.5}},
      {$Duration:1500,x:0.3,y:-0.3,$Delay:20,$Cols:8,$Rows:4,$Clip:15,$During:{$Left:[0.1,0.9],$Top:[0.1,0.9]},$SlideOut:true,$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Assembly:260,$Easing:{$Left:$Jease$.$InJump,$Top:$Jease$.$InJump,$Clip:$Jease$.$OutQuad},$Outside:true,$Round:{$Left:0.8,$Top:2.5}},
      {$Duration:1500,x:0.2,y:-0.1,$Delay:20,$Cols:8,$Rows:4,$Clip:15,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Assembly:260,$Easing:{$Left:$Jease$.$InWave,$Top:$Jease$.$InWave,$Clip:$Jease$.$OutQuad},$Outside:true,$Round:{$Left:0.8,$Top:2.5}},
      {$Duration:1500,x:0.3,y:-0.3,$Delay:80,$Cols:8,$Rows:4,$Clip:15,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$Easing:{$Left:$Jease$.$InJump,$Top:$Jease$.$InJump,$Clip:$Jease$.$OutQuad},$Outside:true,$Round:{$Left:0.8,$Top:2.5}},
      {$Duration:1800,x:1,y:0.2,$Delay:30,$Cols:10,$Rows:5,$Clip:15,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$SlideOut:true,$Reverse:true,$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Assembly:2050,$Easing:{$Left:$Jease$.$InOutSine,$Top:$Jease$.$OutWave,$Clip:$Jease$.$InOutQuad},$Outside:true,$Round:{$Top:1.3}},
      {$Duration:1000,$Delay:30,$Cols:8,$Rows:4,$Clip:15,$SlideOut:true,$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Assembly:2049,$Easing:$Jease$.$OutQuad},
      {$Duration:1000,$Delay:80,$Cols:8,$Rows:4,$Clip:15,$SlideOut:true,$Easing:$Jease$.$OutQuad},
      {$Duration:1000,y:-1,$Cols:12,$Formation:$JssorSlideshowFormations$.$FormationStraight,$ChessMode:{$Column:12}},
      {$Duration:1000,x:-0.2,$Delay:40,$Cols:12,$SlideOut:true,$Formation:$JssorSlideshowFormations$.$FormationStraight,$Assembly:260,$Easing:{$Left:$Jease$.$InOutExpo,$Opacity:$Jease$.$InOutQuad},$Opacity:2,$Outside:true,$Round:{$Top:0.5}},
      {$Duration:2000,y:-1,$Delay:60,$Cols:15,$SlideOut:true,$Formation:$JssorSlideshowFormations$.$FormationStraight,$Easing:$Jease$.$OutJump,$Round:{$Top:1.5}}
    ];

    var jssor_1_options = {
      $AutoPlay: true,
      $SlideshowOptions: {
        $Class: $JssorSlideshowRunner$,
        $Transitions: jssor_1_SlideshowTransitions,
        $TransitionsOrder: 1
      },
      $ArrowNavigatorOptions: {
        $Class: $JssorArrowNavigator$
      },
      $BulletNavigatorOptions: {
        $Class: $JssorBulletNavigator$
      }
    };

    var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

    /*responsive code begin*/
    /*you can remove responsive code if you don't want the slider scales while window resizing*/
    function ScaleSlider() {
        var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
        if (refSize) {
            refSize = Math.min(refSize, 600);
            jssor_1_slider.$ScaleWidth(refSize);
        }
        else {
            window.setTimeout(ScaleSlider, 30);
        }
    }
    ScaleSlider();
    $Jssor$.$AddEvent(window, "load", ScaleSlider);
    $Jssor$.$AddEvent(window, "resize", ScaleSlider);
    $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
    /*responsive code end*/
    };


          