<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>IsleMedia</title>     
     
        <?php require_once('../universal/imports.html'); ?>         
        
        <script type="text/javascript" src="../../externals/bootstrap-3.2.0-dist/js/jquery.min.js"></script>
        <script src="../../externals/banner_rotator_slider/js/jssor.slider-21.1.6.min.js" type="text/javascript"></script>

        <style type="text/css">
            .carousel-indicators li {
                display: inline-block;
                width: 12px;
                height: 12px;
                margin: 10px;
                text-indent: 0;
                cursor: pointer;
                border: none;
                border-radius: 50%;
                background-color: #959595;

            }
            .carousel-indicators .active {
                width: 12px;
                height: 12px;
                margin: 10px;
                border: 1px;
                background-color: #FFFFFF;
                border-radius: 100%;
                box-shadow: inset 0px 0px 1px 0px rgba(0.5,0.5,0.5,0.5);
            }
            .form-control:focus{
                border-color:#CCCCCC;
                box-shadow:0px 1px 1px rgba(0,0,0,0.015) inset, 0px 0px 8px rgba(255, 100, 255, 0);
            }
        </style>                      
    </head>
    <body class="body_background">
      
        <nav class="navbar navbar-inverse navbar-fixed-top" id="general-nav" style='background: rgb(255, 255, 255) url("../../uploads/menu_images/BG3.gif");border:0px;'>
             <div style="margin-left: 15.5%;margin-top: 33px;margin-right: 15.5%;">
               
               <div class="row">
                    <div class="col-md-2"><img src="../../uploads/menu_images/droppedImage_2.png"></div>
                    <div class="col-md-10">
                        
                        <div class="row left_28">           
                            <div class="col-md-9">
                                <img src="../../uploads/menu_images/droppedImage.png" style="margin-top: 58px;">
                            </div>
                            <div class="col-md-3">
                                <p class="paragraph_style">+256 75 206 4714<br>+256 77 907 6542</p>
                                <p class="paragraph_style">info.islemediacorp.com</p>
                                <p class="paragraph_style">info@islemediacorp.com</p>
                            </div>
                        </div>
                        <div class="row left_28">           
                            <div class="col-md-12">
                                <img src="../../uploads/menu_images/droppedImage_1.png" style="margin-top: -20px;width: 100%;">
                            </div>
                        </div>

                        <div class="row" style="margin-right: 22px;">           
                            <div class="col-md-7">
                                
                            </div>
                            <div class="col-md-5">
                                <div class="row" style="margin-top: -12px;margin-right: -90px;">
                                    <div  class="col-md-3  font17">
                                        <a href="index.php" style="text-decoration: none;" class="menu_text_ic2">HOME<span class="left10">|</span></a>
                                    </div>                          
                                    <div  class="col-md-4 font17">
                                        <a href="service3.php" style="text-decoration: none;color: #AAAAAA;" class="menu_text_ic">&nbsp;SERVICE<span class="left10">|</span></a>

                                    </div>
                                    
                                    <div  class="col-md-4 font17">

                                        <a href="profile.php" style="text-decoration: none;color: #AAAAAA;" class="menu_text_ic">PROFILE<span class="left10">|</span></a>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                    </div>
               </div>
               
             </div>
        </nav>
        <div id="includedContent"></div>
        <!-- Entire page section -->
            <div class="top_body_space2"></div>
            <div class="main_section"><!-- Main page section-->

                <div class="row">
                    <div class="col-md-12">

                        <div style="float: left;width:62.5%;height:auto;margin-bottom: 20px;">
                            <div style="background-color: #ffffff;height: 400px;width: 100%;margin-bottom: 10px;">

                                <div id="carousel-example-generic" style="height: 100%;" class="carousel slide" data-ride="carousel">

                                    <div class="carousel-inner" style="height: 100%;" >

                                      <div class="item active" style="height: 100%;">
                                        <img style="height:400px;width: 100%;" src="../../externals/slider/horizontal/images/2.jpg" alt="islemedia">

                                      </div>
                                      <div class="item" style="height: 100%;">
                                        <img style="height:100%;" src="../../externals/slider/horizontal/images/3.jpg" alt="islemedia">

                                      </div>
                                      <div class="item" style="height: 100%;">
                                        <img style="height:100%;" src="../../externals/slider/horizontal/images/4.jpg" alt="islemedia">

                                      </div>
                                      <div class="item" style="height: 100%;">
                                        <img style="height:100%;" src="../../externals/slider/horizontal/images/5.jpg" alt="islemedia">

                                      </div>


                                    </div>
                                <!-- Controls -->
                                <ol class="carousel-indicators" style="height:70%;padding-top:250px;">
                                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                  <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                  <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                  <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                                </ol>
                              </div>
                                
                            </div>

                            <div style="background-color: #ffffff;height: 450px;width: 100%;margin-bottom: 7px;">

                               <ul class="enlarge">
                                    <li>
                                        <img src="../../externals/images/row1_col1_sect2/1.jpg" style="width:100%;" alt="Dechairs" />
                                            <span style="width: 107%;bottom:0px;margin-top: 60%;margin-left: -20px;">
                                                <!-- <img style="width: 280px;" src="../../externals/images/row1_col1_sect2/1.jpg" alt="Deckchairs" /><br /> -->
                                                just description text T
                                            </span>
                                    </li>                                    
                                </ul>

                            </div>

                           <!--  <div style="height: 450px;width: 100%;margin-bottom: 7px;"> -->
                            <div class="row" style="margin-left: 0%;">
                                <div class="col-md-6 col-sm-12 col-xs-12 " style="width: 48.5%;margin-right: 8px;">
                                    <ul class="enlarge">
                                        <li>
                                            <img src="../../externals/images/row1_col1_sect31/1.png" style="width:128%;margin-left: -55px;" alt="Dechairs" />
                                                <span style="width: 100%;bottom:0px;margin-top: 60%;margin-left: -30px;width: 123%;">                                                  
                                                    just description text Nomis
                                                </span>
                                        </li>                                    
                                    </ul>
                                </div>
                                
                                <div class="col-md-6 col-sm-12 col-xs-12 " style="width: 48.5%;margin-right: 8px;">
                                    <ul class="enlarge">
                                        <li>
                                            <img src="../../externals/images/row1_col1_sect32/1.png" style="width:131.7%;margin-left: -62px;" alt="Dechairs" />
                                                <span style="width: 100%;bottom:0px;margin-top: 60%;margin-left: -42px;width: 131.7%;">
                                                    <!-- <img style="width: 280px;" src="../../externals/images/row1_col1_sect32/1.png" alt="Deckchairs" /><br /> -->
                                                    just description text T
                                                </span>
                                        </li>                                    
                                    </ul>
                                </div>
                            </div>
                           <!--  </div> -->

                           <!-- <div class="row" style="margin-left: 0%;">
                                <div class="col-md-6 col-sm-12 col-xs-12 " style="background-color: #ffffff;width: 48.5%;margin-right: 8px;">Nomis</div>
                                <div class="col-md-6 col-sm-12 col-xs-12" style="background-color: #ffffff;width: 47.8%"> Wilson</div>
                            </div>

                            -->

                            <div class="row" style="margin-left: 0%;">
                                <div class="col-md-12 col-sm-12 col-xs-12 " style="width: 87.5%;margin-right: 8px;">
                                    <ul class="enlarge">
                                        <li>
                                            <img src="../../externals/images/row1_col1_sect4/1.png" style="width:128%;margin-left: -55px;" alt="Dechairs" />
                                                <span style="width: 100%;bottom:0px;margin-top: 60%;margin-left: -29px;width: 125.5%;">
                                                    <!-- <img style="width: 280px;" src="../../externals/images/row1_col1_sect4/1.png" alt="Deckchairs" /><br /> -->
                                                    just description text 23 just description text 23 just description text 23 just description text 23 just description text 23 just description text 23 just description text 23 just description text 23   
                                                </span>
                                        </li>                                    
                                    </ul>
                                </div>
                            </div>





                        </div>



                        <div style="float: right;width:36.5%;height:auto;margin-bottom: 7px;">

                            <div style="background-color: #ffffff;height: 320px;width: 100%;margin-bottom: 10px;">
                                <!-- start -->
                                <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 600px; height: 550px; overflow: hidden; visibility: hidden;">
                                    
                                    <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 600px; height:550px; overflow: hidden;">
                                        <div data-p="112.50">
                                            <img data-u="image" src="../../externals/banner_rotator_slider/img/Blue-Diary.png" />
                                        </div>
                                        <a data-u="any" href="http://www.jssor.com" style="display:none">Nomix</a>
                                        <div data-p="112.50" style="display: none;">
                                            <img data-u="image" src="../../externals/banner_rotator_slider/img/2.jpg" />
                                        </div>
                                        <div data-p="112.50" style="display: none;">
                                            <img data-u="image" src="../../externals/banner_rotator_slider/img/3.jpg" />
                                        </div>
                                        <div data-p="112.50" style="display: none;">
                                            <img data-u="image" src="../../externals/banner_rotator_slider/img/04.jpg" />
                                        </div>
                                    </div>
                                    <!-- Bullet Navigator -->
                                    <div data-u="navigator" class="jssorb01" style="bottom:16px;right:16px;">
                                        <div data-u="prototype" style="width:12px;height:12px;"></div>
                                    </div>
                                    <!-- Arrow Navigator -->
                                    <span data-u="arrowleft" class="jssora05l" style="top:0px;left:8px;width:40px;height:40px;" data-autocenter="2"></span>
                                    <span data-u="arrowright" class="jssora05r" style="top:0px;right:8px;width:40px;height:40px;" data-autocenter="2"></span>
                                </div>
                                <!-- End -->
                            </div>

                            <div style="background-color: #ffffff;height: 320px;width: 100%;margin-bottom: 10px;">
                                <!-- start -->
                                <ul class="enlarge">
                                    <li>
                                        <img src="../../externals/images/row1_col2_sect2/1.jpg" style="width:112%;margin-left: -40px;height: 300px;" alt="Dechairs" />
                                            <span style="width: 112.5%;bottom:-20px;margin-top: 60%;margin-left: -20px;">
                                                <!-- <img style="width: 280px;" src="../../externals/images/row1_col2_sect2/1.jpg" alt="Deckchairs" /><br /> -->
                                                No need for colliming Malaria
                                            </span>
                                    </li>                                    
                                </ul>
                                <!-- End -->
                            </div>
                            <div style="background-color: #ffffff;height: 320px;width: 100%;margin-bottom: 10px;">

                                <!-- start -->
                                <ul class="enlarge">
                                    <li>
                                        <img src="../../externals/images/row1_col2_sect3/1.jpg" style="width:112%;margin-left: -40px;height: 300px;" alt="Dechairs" />
                                            <span style="width: 112.5%;bottom:-20px;margin-top: 60%;margin-left: -20px;">
                                                <!-- <img style="width: 280px;" src="../../externals/images/row1_col2_sect3/1.jpg" alt="Deckchairs" /><br /> -->
                                                No need for colliming
                                            </span>
                                    </li>                                    
                                </ul>
                                <!-- End -->
                                
                            </div>

                            <div style="background-color: #ffffff;height: 310px;width: 100%;margin-bottom: 10px;">

                                <!-- start -->
                                <ul class="enlarge">
                                    <li>
                                        <img src="../../externals/images/row1_col2_sect4/1.jpg" style="width:112%;margin-left: -40px;height: 300px;" alt="Dechairs" />
                                            <span style="width: 112.5%;bottom:-10px;margin-top: 60%;margin-left: -20px;">
                                                <!-- <img style="width: 280px;" src="../../externals/images/row1_col2_sect4/1.jpg" alt="Deckchairs" /><br /> -->
                                                No need for colliming final
                                            </span>
                                    </li>                                    
                                </ul>
                                <!-- End -->
                                
                            </div>

                            <!-- <div style="height: 200px;width: 100%;margin-bottom: 10px;"> -->

                                <!-- start -->
                                <ul class="enlarge">
                                    <li>
                                        <img src="../../externals/images/row1_col2_sect5/1.jpg" style="width:112%;margin-left: -40px;height: 200px;background-color: #ADACB1;" alt="Dechairs" />
                                            <span style="width: 112%;bottom:0px;margin-top: 40%;margin-left: -20px;">
                                                <!-- <img style="width: 280px;" src="../../externals/images/row1_col2_sect5/1.jpg" alt="Deckchairs" /><br /> -->
                                                No need for colliming
                                            </span>
                                    </li>                                    
                                </ul>
                                <!-- End -->
                                
                           <!--  </div> -->
                            
                        </div>


                       
                    </div>
                </div>
            <!--End of Row 1 -->

            <!-- Row 1 -->
            <div class="row" style="">
                <div class="col-md-12">
                    <ul class="enlarge" style="margin-top: -20px;">
                        <li>
                            <img src="../../externals/images/other/shapeimage_4.png" style="width:104%;margin-left: -40px;height: 400px;" alt="Dechairs" />
                                <span style="width: 104%;bottom:0px;margin-top: 30%;margin-left: -20px;">
                                    <!-- <img style="width: 280px;" src="../../externals/images/other/shapeimage_4.png" alt="Deckchairs" /><br /> -->
                                    No need for colliming 
                                </span>
                        </li>                                    
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <ul class="enlarge">
                        <li>
                            <img src="../../externals/images/other/Flag-32.jpg" style="width:114.4%;margin-left: -40px;height: 310px;" alt="Dechairs" />
                                <span style="width: 112%;bottom:0px;margin-top: 50%;margin-left: -15px;">
                                    <!-- <img style="width: 280px;" src="../../externals/images/other/Flag-32.jpg" alt="Deckchairs" /><br /> -->
                                    No need for colliming final
                                </span>
                        </li>                                    
                    </ul>
                </div>
                <div class="col-md-6" style="">
                    <div class="row">
                        <div class="col-md-5">
                            <ul class="enlarge">
                                <li>
                                    <img src="../../externals/images/other/engrave.jpg" style="width:145%;margin-left: -40px;height: 150px;" alt="Dechairs" />
                                        <span style="width: 145%;bottom:0px;margin-top: 70%;margin-left: -20px;">
                                            <!-- <img style="width: 280px;" src="../../externals/images/other/engrave.jpg" alt="Deckchairs" /><br /> -->

                                            No need for colliming final
                                        </span>
                                </li>                                    
                            </ul>
                        </div>
                        <div class="col-md-7">
                            <ul class="enlarge">
                                <li>
                                    <img src="../../externals/images/other/shapeimage_5.png" style="width:117%;margin-left: -40px;height: 150px;" alt="Dechairs" />
                                        <span style="width: 117%;bottom:0px;margin-top: 40%;margin-left: -20px;">
                                            <!-- <img style="width: 280px;" src="../../externals/images/other/shapeimage_5.png" alt="Deckchairs" /><br /> -->
                                            No need for colliming final
                                        </span>
                                </li>                                    
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="enlarge">
                                <li>
                                    <img src="../../externals/images/other/shapeimage_6.png" style="width:108.6%;margin-left: -40px;height: 150px;" alt="Dechairs" />
                                        <span style="width: 108%;bottom:0px;margin-top: 25%;margin-left: -20px;">
                                            <!-- <img style="width: 280px;" src="../../externals/images/other/shapeimage_6.png" alt="Deckchairs" /><br /> -->
                                            No need for colliming final
                                        </span>
                                </li>                                    
                            </ul>
                        </div>
                        
                    </div>
                </div>
            </div>

            <div class="row" style="">
                <div class="col-md-12">
                    <ul class="enlarge" style="margin-top: 0px;">
                        <li>
                            <img src="../../externals/images/other/Standees.jpg" style="width:104%;margin-left: -40px;height: 350px;" alt="Dechairs" />
                                <span style="width: 102.7%;bottom:0px;margin-top: 25%;margin-left: -14px;">
                                    <!-- <img style="width: 280px;" src="../../externals/images/other/Standees.jpg" alt="Deckchairs" /><br /> -->
                                    No need for colliming 
                                </span>
                        </li>                                    
                    </ul>
                </div>
            </div>

            <div class="row" style="">
                <div class="col-md-8">
                   <p class="white paragraph_style_2">2016 Copyrighted. Produced & Designed by:<br>Isle Media Corporations Ltd.</p>
                </div>
                <div class="col-md-4">
                   <p class="white paragraph_style_2" style="text-align:right;">Contact:<br> tel:  &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +256 75 206 4714 <br>+256 77 907 6542<br>email:  <a title="mailto:islemedia@gmail.com" class="white" href="mailto:islemedia@gmail.com"><u>islemedia@gmail.com</u></a><br>info.islemediacorp.com

                   </p>
                </div>
            </div>

            </div><!-- End of Main page section-->


	        <script src="../../externals/bootstrap-3.2.0-dist/js/bootstrap.min.js"></script> 
            <script src="../../js/docs.js"></script>
           
            <script type="text/javascript">jssor_1_slider_init();</script>


	        
    </body>
</html>