<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>IsleMedia</title>     
        <!-- <link rel="import" href="../universal/imports.html"> -->
        <?php require_once('../universal/imports.html'); ?> 
               
        <link rel = "stylesheet" href = "../../css/vertical_slider.css">
        <link rel="stylesheet" type="text/css" href="../../externals/slider/horizontal/styles/style.css" /> 
        <script type="text/javascript" src="../../externals/bootstrap-3.2.0-dist/js/jquery.min.js"></script>

        <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
        <script src="../../externals/banner_rotator_slider/js/jssor.slider-21.1.6.min.js" type="text/javascript"></script>

        <script src="../../externals/ImagepopOver/libs/modernizr.js" type="text/javascript"></script><style type="text/css">
            .paragraph_style_2 {
                color: rgb(92, 93, 96);
                font-family: 'Helvetica', 'Arial', sans-serif;
                font-size: 12px;
                font-stretch: normal;
                font-style: normal;
                font-variant: normal;
                font-weight: 400;
                letter-spacing: 0;
                line-height: 17px;
                margin-bottom: 0px;
                margin-left: 0px;
                margin-right: 0px;
                margin-top: 0px;
                opacity: 1.00;
                padding-bottom: 0px;
                padding-top: 0px;
                text-align: left;
                text-decoration: none;
                text-indent: 0px;
                text-transform: none;
            }
            .myp{
           
                display: block;
                -webkit-margin-before: 1em;
                -webkit-margin-after: 1em;
                -webkit-margin-start: 0px;
                -webkit-margin-end: 0px;

            }
            .white{color: #ffffff;}

             .carousel-indicators li {
                display: inline-block;
                width: 12px;
                height: 12px;
                margin: 10px;
                text-indent: 0;
                cursor: pointer;
                border: none;
                border-radius: 50%;
                background-color: #959595;

            }
            .carousel-indicators .active {
                width: 12px;
                height: 12px;
                margin: 10px;
                border: 1px;
                background-color: #FFFFFF;
                border-radius: 100%;
                box-shadow: inset 0px 0px 1px 0px rgba(0.5,0.5,0.5,0.5);
            }
            .form-control:focus{
                border-color:#CCCCCC;
                box-shadow:0px 1px 1px rgba(0,0,0,0.015) inset, 0px 0px 8px rgba(255, 100, 255, 0);
            }
        </style>                                        
    </head>
    <body class="body_background">
        <!-- <link rel="import" href="../universal/top_header.html"> -->
        <nav class="navbar navbar-inverse navbar-fixed-top" id="general-nav" style='background: rgb(255, 255, 255) url("../../uploads/menu_images/BG3.gif");border:0px;'>
           <div style="margin-left: 15.5%;margin-top: 33px;margin-right: 15.5%;">
             
             <div class="row">
                <div class="col-md-2"><img src="../../uploads/menu_images/droppedImage_2.png"></div>
                <div class="col-md-10">
                  
                  <div class="row left_28">       
                    <div class="col-md-9">
                      <img src="../../uploads/menu_images/droppedImage.png" style="margin-top: 58px;">
                    </div>
                    <div class="col-md-3">
                      <p class="paragraph_style">+256 75 206 4714<br>+256 77 907 6542</p>
                      <p class="paragraph_style">info.islemediacorp.com</p>
                      <p class="paragraph_style">info@islemediacorp.com</p>
                    </div>
                  </div>
                  <div class="row left_28">       
                    <div class="col-md-12">
                      <img src="../../uploads/menu_images/droppedImage_1.png" style="margin-top: -20px;width: 100%;">
                    </div>
                  </div>

                  <div class="row" style="margin-right: 22px;">       
                    <div class="col-md-7">
                      
                    </div>
                    <div class="col-md-5">
                      <div class="row" style="margin-top: -12px;margin-right: -90px;">
                        <div  class="col-md-3  font17">
                          <a href="../../index.php" style="text-decoration: none;color: #AAAAAA;" class="menu_text_ic2">HOME<span class="left10">|</span></a>
                        </div>                
                        <div  class="col-md-4 font17">
                          <a href="service3.php" style="text-decoration: none;color: #AAAAAA;" class="menu_text_ic">&nbsp;SERVICE<span class="left10">|</span></a>

                        </div>
                        
                        <div  class="col-md-4 font17">

                          <a href="profile.php" style="text-decoration: none;color: #ffffff;" class="">PROFILE<span class="left10">|</span></a>
                        </div>
                        
                      </div>
                    </div>
                  </div>

                </div>
             </div>
             
           </div>
        </nav>
        <div id="includedContent"></div>
        <!-- Entire page section -->
            <div class="top_body_space2"></div>
            <div class="main_section"><!-- Main page section-->

            <!-- <div class="row" style="">
                <div class="col-md-12">
                  <div style="background-color: #ffffff;padding-left: 81px;">
                        <p style="padding-bottom: 0pt; ;color: #806F6F; font-size:22px;" class="paragraph_style_1">Who we are</p>

                        <img src="../../externals/images/other/shapeimageLine.png" style="margin-top: -38px;">
                  </div>
                </div>
              </div> -->

             <div class="row" style="">
                <div class="col-md-12">
                  <div style="background-color: #fff;padding-left: 0px;padding-top: 0px">

                      <div id="carousel-example-generic" style="height: 100%;" class="carousel slide" data-ride="carousel">

                          <div class="carousel-inner" style="height: 100%;" >

                            <div class="item active" style="height: 400px;">
                              <img style="height:400px;width: 100%;" src="../../externals/slider/horizontal/images/2.jpg" alt="islemedia">

                            </div>
                            <div class="item" style="height: 100%;">
                              <img style="height:400px;width: 100%;" src="../../externals/slider/horizontal/images/3.jpg" alt="islemedia">

                            </div>
                            <div class="item" style="height: 100%;">
                              <img style="height:400px;width: 100%;" src="../../externals/slider/horizontal/images/4.jpg" alt="islemedia">

                            </div>
                            <div class="item" style="height: 100%;">
                              <img style="height:400px;width: 100%;" src="../../externals/slider/horizontal/images/5.jpg" alt="islemedia">

                            </div>


                          </div>
                      <!-- Controls -->
                      <ol class="carousel-indicators" style="height:70%;padding-top:250px;">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                      </ol>
                    </div>
                  </div>
                </div>
              </div>

            <!-- Row 1 -->
            <div class="row" style="">
                <div class="col-md-12">
                    <div style="background-color: #ffffff;padding-left: 81px;">
                        <p style="padding-bottom: 0pt; ;color: #806F6F; font-size:22px;" class="paragraph_style_1">Who we are</p>

                        <img src="../../externals/images/other/shapeimageLine.png" style="margin-top: -38px;">

                        <p class="myp" style="color: #707070;font-weight: bold;font-size: 16px;padding-bottom: 0pt; padding-top: 0pt;margin-top: -18px; ">We are an innovative and entrepreneurial company providing digital <br>marketing solutions</p>

                        <div class="graphic_textbox_layout_style_default" style="margin-bottom: 10px;">
                          <p style="padding-top: 0pt; " class="paragraph_style_2">Isle Media Corporations Ltd. is a multi award-winning website design and digital marketing company recognized <br /></p>
                          <p class="paragraph_style_2">as the ‘go to’ company for high-end website design, development and tactical digital marketing campaigns.<br /></p>
                          <p class="paragraph_style_2"><br /></p>
                          <p class="paragraph_style_2">We understand that what our clients want are websites that really catch the eye - but also ones work precisely <br /></p>
                          <p class="paragraph_style_2">the way your business needs them to; sites that your clients and customers love to.<br /></p>
                          <p class="paragraph_style_2"><br /></p>
                          <p class="paragraph_style_2">Working with us, you’ll soon start to view Isle Media Corporations Ltd. as a valuable extension of your own marketing<br /></p>
                          <p class="paragraph_style_2">team.<br /></p>
                          <p class="paragraph_style_2">We’ll quickly get to fully understand your business, the marketplace in which you operate and the way you want <br /></p>
                          <p class="paragraph_style_2">to interact with your customers or clients.<br /></p>
                          <p class="paragraph_style_2"><br /></p>
                          <p class="paragraph_style_2">With an in-house team of more than 100 specialists always keeping an eye on the latest web development technology,<br /></p>
                          <p class="paragraph_style_2">design and marketing techniques, we’ll constantly deliver new solutions that fit precisely with your ever-changing \<br /></p>
                          <p class="paragraph_style_2">digital requirements and keep you one step ahead of your competitors.<br /></p>
                          <p class="paragraph_style_2"><br /></p>
                          <p class="paragraph_style_2">Our content management systems offer bang up to date functionality while always being easy for our clients to use,<br /></p>
                          <p class="paragraph_style_2">allowing you to edit every aspect of your site yourself whenever you like - though we’re always on hand with help and <br /></p>
                          <p class="paragraph_style_2">advice you need it.<br /></p>
                          <p class="paragraph_style_2"><br /></p>
                          <p class="paragraph_style_2">We can also work with you to carry your enhanced profile through to your offline presence, with eye-catching company <br /></p>
                          <p class="paragraph_style_2">branding, brochures and exhibition materials designed and written by our in-house team of experienced and inventive <br /></p>
                          <p class="paragraph_style_2">graphic designers and copywriters. We’ll even handle the print, production and delivery of your promotional or internal <br /></p>
                          <p class="paragraph_style_2">communication materials if you like.<br /></p>
                          <p class="paragraph_style_2"><br /></p>
                          <p class="paragraph_style_2">All of which brings you the results you want while removing the hassle of dealing with different agencies that you could <br /></p>
                          <p class="paragraph_style_2">do without!<br /></p>
                          <p class="paragraph_style_2"><br /></p>
                          <p class="paragraph_style_2">At Isle Media Corporations Ltd. we encourage an open and positive team culture from which naturally emerges <br /></p>
                          <p class="paragraph_style_2">creativity, innovation and genuine excitement about the results we bring for our clients.<br /></p>
                          <p class="paragraph_style_2"><br /></p>
                          <p class="paragraph_style_2">That philosophy of openness and true teamwork is reflected in our relationships with all our clients where exceptional <br /></p>
                          <p class="paragraph_style_2">customer service and transparent service level agreements are guaranteed.<br /></p>
                          <p class="paragraph_style_2"><br /></p>
                          <p style="padding-bottom: 0pt; " class="paragraph_style_2">We’re proud to have built strong and lasting partnerships with many companies and this is shown by what our clients say.</p>
                         <div style="height: 140px;"></div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row" style="">
                <div class="col-md-8">
                   <p class="white paragraph_style_2">2016 Copyrighted. Produced & Designed by:<br>Isle Media Corporations Ltd.</p>
                </div>
                <div class="col-md-4">
                   <p class="white paragraph_style_2" style="text-align:right;">Contact:<br> tel:  &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +256 75 206 4714 <br>+256 77 907 6542<br>email:  <a title="mailto:islemedia@gmail.com" class="white" href="mailto:islemedia@gmail.com"><u>islemedia@gmail.com</u></a><br>info.islemediacorp.com

                   </p>
                </div>
            </div>




                
               <?php //require_once("../universal/footer.html"); ?>
            </div><!-- End of Main page section-->



            <script type="text/javascript">
                function get_selected_menu_item(item){
                    window.location="../page_2/page2.php";

                }
            </script>

	        <!-- <script src="../../externals/bootstrap-3.2.0-dist/js/jquery.js"></script> -->
            <!-- <script src="../../externals/bootstrap-3.2.0-dist/js/jquery.min.js"></script> -->
	        <script src="../../externals/bootstrap-3.2.0-dist/js/bootstrap.min.js"></script> 
            <script src="../../js/docs.js"></script>
            <script type="text/javascript" src="../../externals/slider/horizontal/wowslider.js"></script>
            <script type="text/javascript" src="../../externals/slider/horizontal/script.js"></script>  
            <script type="text/javascript">jssor_1_slider_init();</script>


	        
    </body>
</html>