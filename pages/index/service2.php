<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>IsleMedia</title>     
        <!-- <link rel="import" href="../universal/imports.html"> -->
        <?php require_once('../universal/imports.html'); ?> 
               
        <link rel = "stylesheet" href = "../../css/vertical_slider.css">
        <link rel="stylesheet" type="text/css" href="../../externals/slider/horizontal/styles/style.css" /> 
         <link rel="stylesheet" type="text/css" media="screen,print" href="SERVICE___files/SERVICE__.css" />


        <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
        <script src="../../externals/banner_rotator_slider/js/jssor.slider-21.1.6.min.js" type="text/javascript"></script>

        <script src="../../externals/ImagepopOver/libs/modernizr.js" type="text/javascript"></script><style type="text/css">
            .paragraph_style_2 {
                color: rgb(92, 93, 96);
                font-family: 'Helvetica', 'Arial', sans-serif;
                font-size: 12px;
                font-stretch: normal;
                font-style: normal;
                font-variant: normal;
                font-weight: 400;
                letter-spacing: 0;
                line-height: 17px;
                margin-bottom: 0px;
                margin-left: 0px;
                margin-right: 0px;
                margin-top: 0px;
                opacity: 1.00;
                padding-bottom: 0px;
                padding-top: 0px;
                text-align: left;
                text-decoration: none;
                text-indent: 0px;
                text-transform: none;
            }
            .myp{
           
                display: block;
                -webkit-margin-before: 1em;
                -webkit-margin-after: 1em;
                -webkit-margin-start: 0px;
                -webkit-margin-end: 0px;

            }
            .white{color: #ffffff;}
        </style>                                        
    </head>
    <body class="body_background">
        <!-- <link rel="import" href="../universal/top_header.html"> -->
        <?php require_once("../universal/top_header.html"); ?> 
        <div id="includedContent"></div>
        <!-- Entire page section -->
            <div class="top_body_space2"></div>
            <div class="main_section"><!-- Main page section-->


            <!-- Row 1 -->
            <div class="row" style="">
                <div class="col-md-12">
                    <div style="background-color: #ffffff;padding-left: 81px;">
                        

                        <div class="graphic_textbox_layout_style_default">
                          <p style="padding-bottom: 0pt; padding-top: 0pt; " class="paragraph_style_1">BRANDING &amp; PRINT</p>
                        </div>


                        <div class="text-content graphic_textbox_layout_style_default_External_751_374" style="padding: 0px; ">
                          <div class="graphic_textbox_layout_style_default">
                          
                            <p style="padding-top: 0pt; " class="paragraph_style_2">A strong brand is a business critical element in presenting a professional image to the outside world<br /></p>
                            <p class="paragraph_style_2">and helping to both establish and promote credibility and trust. It is the identity that sets you apart from<br /></p>
                            <p class="paragraph_style_2">any competition and represents your underlying personality and values as an organization.<br /></p>
                            <p class="paragraph_style_2">It could be strongly argued that people aside, your brand is your businesses most valuable asset and is<br /></p>
                            <p class="paragraph_style_2">in many ways the driving force behind it. To define it further one could say that your brand is an idea in<br /></p>
                            <p class="paragraph_style_2">the minds of your staff and customers and that idea in turn is formed by what you say (marketing) and<br /></p>
                            <p class="paragraph_style_2">what you do (operations)”.<br /></p>
                            <p class="paragraph_style_2"><br /></p>
                            <p class="paragraph_style_2">More specifically it is:<br /></p>
                            <p class="paragraph_style_2"><br /></p>
                            <p class="paragraph_style_2"><span style="line-height: 20px; " class="style">› </span>A promise<br /></p>
                            <p class="paragraph_style_2"><span style="line-height: 20px; " class="style">› </span>The sensory, emotional and cultural proprietary image surrounding a company, product or service<br /></p>
                            <p class="paragraph_style_2"><span style="line-height: 20px; " class="style">› </span>An assurance of quality and the enhancement of perceived value and satisfaction<br /></p>
                            <p class="paragraph_style_2"><br /></p>
                            <p class="paragraph_style_2">When a strong brand is established, people remember an organization and the attributes that<br /></p>
                            <p class="paragraph_style_2">differentiate it and set it apart from the competition. A strong brand impacts on everything, from the<br /></p>
                            <p class="paragraph_style_2">ability to recruit the best staff through to growing the bottom line.<br /></p>
                            <p class="paragraph_style_2"><span class="style_1">Isle Media Corporations Ltd.</span>’s designers are specialists in branding and have worked with organizations of all types and<br /></p>
                            <p class="paragraph_style_2">size across a broad range of projects, from brand strategy, logo design and key message development,<br /></p>
                            <p class="paragraph_style_2">through to new trading/product name selection and brand guidelines development.<br /></p>

                          </div>
                          
                        </div>
         

                    </div>
                </div>
            </div>

            <div class="row" style="">
                <div class="col-md-8">
                   <p class="white paragraph_style_2">2016 Copyrighted. Produced & Designed by:<br>Isle Media Corporations Ltd.</p>
                </div>
                <div class="col-md-4">
                   <p class="white paragraph_style_2" style="text-align:right;">Contact:<br> tel:  &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +256 75 206 4714 <br>+256 77 907 6542<br>email:  <a title="mailto:islemedia@gmail.com" class="white" href="mailto:islemedia@gmail.com"><u>islemedia@gmail.com</u></a><br>info.islemediacorp.com

                   </p>
                </div>
            </div>




                
               <?php //require_once("../universal/footer.html"); ?>
            </div><!-- End of Main page section-->



            <script type="text/javascript">
                function get_selected_menu_item(item){
                    window.location="../page_2/page2.php";

                }
            </script>

	        <!-- <script src="../../externals/bootstrap-3.2.0-dist/js/jquery.js"></script> -->
            <!-- <script src="../../externals/bootstrap-3.2.0-dist/js/jquery.min.js"></script> -->
	        <script src="../../externals/bootstrap-3.2.0-dist/js/bootstrap.min.js"></script> 
            <script src="../../js/docs.js"></script>
            <script type="text/javascript" src="../../externals/slider/horizontal/wowslider.js"></script>
            <script type="text/javascript" src="../../externals/slider/horizontal/script.js"></script>  
            <script type="text/javascript">jssor_1_slider_init();</script>


	        
    </body>
</html>