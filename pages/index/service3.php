<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>IsleMedia</title>     
        <!-- <link rel="import" href="../universal/imports.html"> -->
       <link rel = "stylesheet" href = "../../externals/bootstrap-3.2.0-dist/css/bootstrap.min.css">
       <link rel = "stylesheet" href = "../../css/custome_css.css">
               
        <link rel = "stylesheet" href = "../../css/vertical_slider.css">
        
        <link rel="stylesheet" type="text/css" href="../../externals/slider/horizontal/styles/style.css" /> 
         <link rel="stylesheet" type="text/css" media="screen,print" href="SERVICE___files/SERVICE__.css" />

         <link rel = "stylesheet" href = "../../css/sliderstyle.css">
         <link rel = "stylesheet" href = "../../css/bubble_slider.css">
         
        <script type="text/javascript" src="../../externals/bootstrap-3.2.0-dist/js/jquery.min.js"></script><style type="text/css">
            .paragraph_style_2 {
                color: rgb(92, 93, 96);
                font-family: 'Helvetica', 'Arial', sans-serif;
                font-size: 12px;
                font-stretch: normal;
                font-style: normal;
                font-variant: normal;
                font-weight: 400;
                letter-spacing: 0;
                line-height: 17px;
                margin-bottom: 0px;
                margin-left: 0px;
                margin-right: 0px;
                margin-top: 0px;
                opacity: 1.00;
                padding-bottom: 0px;
                padding-top: 0px;
                text-align: left;
                text-decoration: none;
                text-indent: 0px;
                text-transform: none;
            }
            .myp{
           
                display: block;
                -webkit-margin-before: 1em;
                -webkit-margin-after: 1em;
                -webkit-margin-start: 0px;
                -webkit-margin-end: 0px;

            }
            .white{color: #ffffff;}
        </style>                                        
    </head>
    <body class="body_background">
        <!-- <link rel="import" href="../universal/top_header.html"> -->
        <nav class="navbar navbar-inverse navbar-fixed-top" id="general-nav" style='background: rgb(255, 255, 255) url("../../uploads/menu_images/BG3.gif");border:0px;'>
             <div style="margin-left: 15.5%;margin-top: 33px;margin-right: 15.5%;">
               
               <div class="row">
                    <div class="col-md-2"><img src="../../uploads/menu_images/droppedImage_2.png"></div>
                    <div class="col-md-10">
                        
                        <div class="row left_28">           
                            <div class="col-md-9">
                                <img src="../../uploads/menu_images/droppedImage.png" style="margin-top: 58px;">
                            </div>
                            <div class="col-md-3">
                                <p class="paragraph_style">+256 75 206 4714<br>+256 77 907 6542</p>
                                <p class="paragraph_style">info.islemediacorp.com</p>
                                <p class="paragraph_style">info@islemediacorp.com</p>
                            </div>
                        </div>
                        <div class="row left_28">           
                            <div class="col-md-12">
                                <img src="../../uploads/menu_images/droppedImage_1.png" style="margin-top: -20px;width: 100%;">
                            </div>
                        </div>

                        <div class="row" style="margin-right: 22px;">           
                            <div class="col-md-7">
                                
                            </div>
                            <div class="col-md-5">
                                <div class="row" style="margin-top: -12px;margin-right: -90px;">
                                    <div  class="col-md-3  font17">
                                        <a href="../../index.php" style="text-decoration: none;color: #AAAAAA;" class="menu_text_ic2">HOME<span class="left10">|</span></a>
                                    </div>                          
                                    <div  class="col-md-4 font17">
                                        <a href="service3.php" style="text-decoration: none;" class="menu_text_ic">&nbsp;SERVICE<span class="left10">|</span></a>

                                    </div>
                                    
                                    <div  class="col-md-4 font17">

                                        <a href="profile.php" style="text-decoration: none;color: #AAAAAA;" class="menu_text_ic">PROFILE<span class="left10">|</span></a>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                    </div>
               </div>
               
             </div>
        </nav>
        <div id="includedContent"></div>
        <!-- Entire page section -->
            <div class="top_body_space2"></div>
            <div class="main_section"><!-- Main page section-->


            <!-- Row 1 -->
            <div class="row" style="">
                <div class="col-md-12">
                    <div style="background-color: #ffffff;padding-left: 81px;">
                        

                        <div class="graphic_textbox_layout_style_default">
                          <p style="padding-bottom: 0pt; padding-top: 0pt; " class="paragraph_style_1">BRANDING &amp; PRINT</p>
                        </div>


                        <div class="text-content graphic_textbox_layout_style_default_External_751_374" style="padding: 0px; ">
                          <div class="graphic_textbox_layout_style_default">
                            <div class="row">
                                <div class="col-md-9">
                                    <p style="padding-top: 0pt; " class="paragraph_style_2">A strong brand is a business critical element in presenting a professional image to the outside world<br /></p>
                                    <p class="paragraph_style_2">and helping to both establish and promote credibility and trust. It is the identity that sets you apart from<br /></p>
                                    <p class="paragraph_style_2">any competition and represents your underlying personality and values as an organization.<br /></p>
                                    <p class="paragraph_style_2">It could be strongly argued that people aside, your brand is your businesses most valuable asset and is<br /></p>
                                    <p class="paragraph_style_2">in many ways the driving force behind it. To define it further one could say that your brand is an idea in<br /></p>
                                    <p class="paragraph_style_2">the minds of your staff and customers and that idea in turn is formed by what you say (marketing) and<br /></p>
                                    <p class="paragraph_style_2">what you do (operations)”.<br /></p>
                                </div>
                                <div class="col-md-3">
                                    
                                    <div class='csslider1 autoplay ' style="margin-left: -27px;">
                                        <input name="cs_anchor1" id='cs_slide1_0' type="radio" class='cs_anchor slide' >
                                        <input name="cs_anchor1" id='cs_slide1_1' type="radio" class='cs_anchor slide' >
                                        <input name="cs_anchor1" id='cs_slide1_2' type="radio" class='cs_anchor slide' >
                                        <input name="cs_anchor1" id='cs_play1' type="radio" class='cs_anchor' checked>
                                        <input name="cs_anchor1" id='cs_pause1' type="radio" class='cs_anchor' >
                                        <ul>
                                            <div style="width: 100%; visibility: hidden; font-size: 0px; line-height: 0;">
                                                <img src="../../externals/slider/horizontal/images/2.jpg" style="width: 100%;">
                                            </div>
                                            <li class='num0 img'>
                                                 <a href="http://cssslider.com" target=""><img src='../../externals/slider/horizontal/images/3.jpg' alt='Buns' title='Buns' /> </a> 
                                            </li>
                                            <li class='num1 img'>
                                                 <a href="http://cssslider.com" target=""><img src='../../externals/slider/horizontal/images/4.jpg' alt='Croissant' title='Croissant' /> </a> 
                                            </li>
                                            <li class='num2 img'>
                                                 <a href="http://cssslider.com" target=""><img src='../../externals/slider/horizontal/images/5.jpg' alt='Lemon pie' title='Lemon pie' /> </a> 
                                            </li>
                                        
                                        </ul>
                                        <!-- <div class='cs_description'>
                                            <label class='num0'>
                                                <span class="cs_title"><span class="cs_wrapper">Buns</span></span>
                                            </label>
                                            <label class='num1'>
                                                <span class="cs_title"><span class="cs_wrapper">Croissant</span></span>
                                            </label>
                                            <label class='num2'>
                                                <span class="cs_title"><span class="cs_wrapper">Lemon pie</span></span>
                                            </label>
                                        </div> -->
                                        
                                        <!-- <div class='cs_arrowprev'>
                                            <label class='num0' for='cs_slide1_0'></label>
                                            <label class='num1' for='cs_slide1_1'></label>
                                            <label class='num2' for='cs_slide1_2'></label>
                                        </div>
                                        <div class='cs_arrownext'>
                                            <label class='num0' for='cs_slide1_0'></label>
                                            <label class='num1' for='cs_slide1_1'></label>
                                            <label class='num2' for='cs_slide1_2'></label>
                                        </div> -->
                                        
                                        <div class='cs_bullets'>
                                            <label class='num0' for='cs_slide1_0'>
                                                <span class='cs_point'></span>
                                                <span class='cs_thumb'><img src='../../externals/slider/horizontal/images/2.jpg' alt='Buns' title='Buns' /></span>
                                            </label>
                                            <label class='num1' for='cs_slide1_1'>
                                                <span class='cs_point'></span>
                                                <span class='cs_thumb'><img src='../../externals/slider/horizontal/images/3.jpg' alt='Croissant' title='Croissant' /></span>
                                            </label>
                                            <label class='num2' for='cs_slide1_2'>
                                                <span class='cs_point'></span>
                                                <span class='cs_thumb'><img src='../../externals/slider/horizontal/images/4.jpg' alt='Lemon pie' title='Lemon pie' /></span>
                                            </label>
                                        </div>
                                    </div>
                                        <!-- End cssSlider.com -->

                                </div>
                            </div>
                            <p class="paragraph_style_2"><br /></p>
                            <p class="paragraph_style_2">More specifically it is:<br /></p>
                            <p class="paragraph_style_2"><br /></p>
                            <p class="paragraph_style_2"><span style="line-height: 20px; " class="style">› </span>A promise<br /></p>
                            <p class="paragraph_style_2"><span style="line-height: 20px; " class="style">› </span>The sensory, emotional and cultural proprietary image surrounding a company, product or service<br /></p>
                            <p class="paragraph_style_2"><span style="line-height: 20px; " class="style">› </span>An assurance of quality and the enhancement of perceived value and satisfaction<br /></p>
                            <p class="paragraph_style_2"><br /></p>
                            <p class="paragraph_style_2">When a strong brand is established, people remember an organization and the attributes that<br /></p>
                            <p class="paragraph_style_2">differentiate it and set it apart from the competition. A strong brand impacts on everything, from the<br /></p>
                            <p class="paragraph_style_2">ability to recruit the best staff through to growing the bottom line.<br /></p>
                            <p class="paragraph_style_2"><span class="style_1">Isle Media Corporations Ltd.</span>’s designers are specialists in branding and have worked with organizations of all types and<br /></p>
                            <p class="paragraph_style_2">size across a broad range of projects, from brand strategy, logo design and key message development,<br /></p>
                            <p class="paragraph_style_2">through to new trading/product name selection and brand guidelines development.<br /></p>

                          </div>
                          
                        </div>


                        <div class="graphic_textbox_layout_style_default">
                          <p style="padding-bottom: 0pt; padding-top: 0pt; " class="paragraph_style_4">LOGO DESIGN</p>
                        </div>

                         <div class="text-content graphic_textbox_layout_style_default_External_751_117" style="padding: 0px; ">
                            <div class="graphic_textbox_layout_style_default">

                              <div class="row">
                                <div class="col-md-9">
                                    <p style="padding-top: 0pt; " class="paragraph_style_5">If you would like to create a new brand identity or refresh an existing logo, our skilled graphic design<br /></p>
                                    <p class="paragraph_style_5">team is ideally placed to help. They will work with you through the various stages of development, from<br /></p>
                                    <p class="paragraph_style_5">the initial brief, ideas and concepts through to the final version presented in various formats for use<br /></p>
                                    <p class="paragraph_style_5">across multiple platforms.<br /></p>
                                    <p class="paragraph_style_5">Our experience in logo design is based on a broad frame of reference covering both the public and<br /></p>
                                    <p style="padding-bottom: 0pt; " class="paragraph_style_5">private sector and a wide range of industry sectors and business sizes.</p>
                                </div>
                                <div class="col-md-3">
                                    <div id="carousel-example-generic" style="height: 140px;width:198px;margin-left: -27px;" class="carousel slide" data-ride="carousel">

                                        <div class="carousel-inner" style="height: 100%;" >

                                          <div class="item active" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../uploads/ads/ojok.png" alt="islemedia">

                                          </div>
                                          <div class="item" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../uploads/ads/1.jpg" alt="islemedia">

                                          </div>
                                          <div class="item" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../uploads/ads/2.jpg" alt="islemedia">

                                          </div>
                                          <div class="item" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../uploads/ads/3.jpg" alt="islemedia">

                                          </div>


                                        </div>
                                        <!-- Controls -->
                                        <ol class="carousel-indicators" style="height:70%;padding-top:250px;">
                                          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                                        </ol>
                                    </div>

                                </div>
                              </div>
                            </div>
                          </div>


                          <div class="graphic_textbox_layout_style_default">
                              <p style="padding-bottom: 0pt; padding-top: 0pt; " class="paragraph_style_6">GRAPHIC DESIGN</p>
                            </div>

                            <div class="text-content graphic_textbox_layout_style_default_External_751_144" style="padding: 0px; ">
                            <div class="graphic_textbox_layout_style_default">

                              <div class="row">
                                <div class="col-md-9">

                                    <p style="padding-top: 0pt; " class="paragraph_style_7">Once a new brand identity has been established there is very often the requirement to develop an<br /></p>
                                    <p class="paragraph_style_7">updated ‘house style’ for all your marketing materials as part of the ‘roll out’ of the new identity.<br /></p>
                                    <p class="paragraph_style_7">Our multi-disciplined design team is able to provide an end-to-end solution in this regard -<br /></p>
                                    <p class="paragraph_style_7">encompassing design, print, production, stock selection, art direction and fulfilment across any number<br /></p>
                                    <p class="paragraph_style_7">of channels. From printed materials such as brochures, prospectus and stationary, through to outdoor<br /></p>
                                    <p class="paragraph_style_7">and digital advertising, infographics, exhibition stands, promotional goods and much more. Please take<br /></p>
                                    <p class="paragraph_style_7">a look at our work in our online portfolio. Alternatively speak to one of our brand specialists by calling<br /></p>
                                    <p style="padding-bottom: 0pt; " class="paragraph_style_7">us on <span class="style_2">+256 75 206 4714 / +256 77 907 6542</span>. We look forward to hearing from you.</p>
                                </div>
                                <div class="col-md-3">

                                  <div id="carousel-example-generic" style="height: 140px;width:198px;margin-left: -27px;" class="carousel slide" data-ride="carousel">

                                        <div class="carousel-inner" style="height: 100%;" >

                                          <div class="item active" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../externals/slider/horizontal/images/2.jpg" alt="islemedia">

                                          </div>
                                          <div class="item" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../externals/slider/horizontal/images/3.jpg" alt="islemedia">

                                          </div>
                                          <div class="item" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../externals/slider/horizontal/images/4.jpg" alt="islemedia">

                                          </div>
                                          <div class="item" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../externals/slider/horizontal/images/5.jpg" alt="islemedia">

                                          </div>


                                        </div>
                                        <!-- Controls -->
                                        <ol class="carousel-indicators" style="height:70%;padding-top:250px;">
                                          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                                        </ol>
                                    </div>
                                </div>
                              </div>
                            </div>
                          </div>


                        <div class="graphic_textbox_layout_style_default">
                            <p style="padding-bottom: 0pt; padding-top: 0pt; " class="paragraph_style_8">PHOTOGRAPHY</p>
                        </div>

                        <div class="text-content graphic_textbox_layout_style_default_External_751_221" style="padding: 0px; ">
                            <div class="graphic_textbox_layout_style_default">

                              <div class="row">
                                <div class="col-md-9">
                                    <p style="padding-top: 0pt; " class="paragraph_style_9"><span class="style_1">Isle Media Corporations Ltd</span> offers expert commercial photography services as part of its comprehensive package for<br /></p>
                                    <p class="paragraph_style_9">clients large or small.<br /></p>
                                    <p class="paragraph_style_9">We have our own in-house photographers, experienced in all aspects of commercial imagery, or we can<br /></p>
                                    <p class="paragraph_style_9">work with other professionals in creating photo-briefs and shot-lists for each new assignment.<br /></p>
                                    <p class="paragraph_style_9">Previously we have provided stunning images for clients in the hotel and leisure trade, sports and<br /></p>
                                    <p class="paragraph_style_9">fitness clubs, food and drink businesses, packaging companies, estate agents, solicitors and IT<br /></p>
                                    <p class="paragraph_style_9">specialists.<br /></p>
                                    <p class="paragraph_style_9">We'll make sure your pictures have an eye-catching appeal so that your websites and brochures will<br /></p>
                                    <p class="paragraph_style_9">stand out from your competitors.<br /></p>
                                    <p class="paragraph_style_9">Our photographers work collaboratively with our designers and copywriters to ensure the images we<br /></p>
                                    <p class="paragraph_style_9">provide are precisely what your project requires. If you would like to discuss this further, contact us<br /></p>
                                    <p style="padding-bottom: 0pt; " class="paragraph_style_9">today.</p>
                                </div>
                                <div class="col-md-3">                      
                                    <div id="carousel-example-generic" style="height: 140px;width:198px;margin-left: -27px;" class="carousel slide" data-ride="carousel">

                                        <div class="carousel-inner" style="height: 100%;" >

                                          <div class="item active" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../uploads/ads/ojok.png" alt="islemedia">

                                          </div>
                                          <div class="item" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../uploads/ads/1.jpg" alt="islemedia">

                                          </div>
                                          <div class="item" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../uploads/ads/2.jpg" alt="islemedia">

                                          </div>
                                          <div class="item" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../uploads/ads/3.jpg" alt="islemedia">

                                          </div>


                                        </div>
                                        <!-- Controls -->
                                        <ol class="carousel-indicators" style="height:70%;padding-top:250px;">
                                          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                                        </ol>
                                    </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        <div class="graphic_textbox_layout_style_default">
                          <p style="padding-bottom: 0pt; padding-top: 0pt; " class="paragraph_style_11">VIDEO PRODUCTION</p>
                        </div>

                        <div class="text-content graphic_textbox_layout_style_default_External_637_137" style="padding: 0px; ">
                            <div class="graphic_textbox_layout_style_default">

                              <div class="row">
                                <div class="col-md-9">
                                  <p style="padding-top: 0pt; " class="paragraph_style_12">Television has become such an integral part of our lives, people can now easily access videos on the<br /></p>
                                  <p class="paragraph_style_12">move by using their phones, tablets and computers. Moving images are subsequently a powerful and<br /></p>
                                  <p class="paragraph_style_12">effective way to communicate your message directly to your target market.<br /></p>
                                  <p class="paragraph_style_12">With content marketing fast becoming the foundation of marketing strategies going forward, what<br /></p>
                                  <p class="paragraph_style_12">better time to consider the many videos that could be created for your business?<br /></p>
                                  <p class="paragraph_style_12">Our team will work with you to come up with creative and engaging ideas to suit your audience,<br /></p>
                                  <p style="padding-bottom: 0pt; " class="paragraph_style_12">whatever your industry sector.</p>
                                </div>
                                <div class="col-md-3">
                                    <div id="carousel-example-generic" style="height: 140px;width:198px;margin-left: -27px;" class="carousel slide" data-ride="carousel">

                                        <div class="carousel-inner" style="height: 100%;" >

                                          <div class="item active" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../externals/slider/horizontal/images/2.jpg" alt="islemedia">

                                          </div>
                                          <div class="item" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../externals/slider/horizontal/images/3.jpg" alt="islemedia">

                                          </div>
                                          <div class="item" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../externals/slider/horizontal/images/4.jpg" alt="islemedia">

                                          </div>
                                          <div class="item" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../externals/slider/horizontal/images/5.jpg" alt="islemedia">

                                          </div>


                                        </div>
                                        <!-- Controls -->
                                        <ol class="carousel-indicators" style="height:70%;padding-top:250px;">
                                          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                                        </ol>
                                    </div>
                                </div>
                              </div>
                            </div>
                          </div>


                        <div class="graphic_textbox_layout_style_default">
                          <p style="padding-bottom: 0pt; padding-top: 0pt; " class="paragraph_style_11">New Product Launch</p>
                        </div>

                        <div class="row">
                                <div class="col-md-9">
                                    <div class="text-content graphic_textbox_layout_style_default_External_637_161" style="padding: 0px; ">
                                        <div class="graphic_textbox_layout_style_default">
                                          <p style="padding-top: 0pt; " class="paragraph_style_13">Why not take advantage of accessibility through portable devices by creating a video to enhance your<br /></p>
                                          <p class="paragraph_style_13">product launch, that can be seen anywhere at any time. It could be used as a welcome to customers on<br /></p>
                                          <p class="paragraph_style_13">your website, as a backdrop at an exhibition, to deliver year end reports to investors, or even educate<br /></p>
                                          <p class="paragraph_style_13">staff about new or correct procedures.<br /></p>
                                          <p class="paragraph_style_13">With over 13 years worth of experience, our production team produces creative programming that will<br /></p>
                                          <p class="paragraph_style_13">powerfully deliver your message to your target market, tailored to your budget and deadline.<br /></p>
                                          <p class="paragraph_style_13">Whatever your video needs, <span class="style_1">Isle Media Corporations Ltd</span> has the creativity, skills and experience to deliver your message<br /></p>
                                          <p style="padding-bottom: 0pt; " class="paragraph_style_13">with visual impact and maximum effect.</p>
                                        </div>
                                      </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div id="carousel-example-generic" style="height: 140px;width:198px;margin-left: -27px;" class="carousel slide" data-ride="carousel">

                                        <div class="carousel-inner" style="height: 100%;" >

                                          <div class="item active" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../uploads/ads/ojok.png" alt="islemedia">

                                          </div>
                                          <div class="item" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../uploads/ads/1.jpg" alt="islemedia">

                                          </div>
                                          <div class="item" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../uploads/ads/2.jpg" alt="islemedia">

                                          </div>
                                          <div class="item" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../uploads/ads/3.jpg" alt="islemedia">

                                          </div>


                                        </div>
                                        <!-- Controls -->
                                        <ol class="carousel-indicators" style="height:70%;padding-top:250px;">
                                          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                                        </ol>
                                    </div>
                                  </div>
                              </div>



                          <div class="graphic_textbox_layout_style_default">
                              <p style="padding-bottom: 0pt; padding-top: 0pt; " class="paragraph_style_15">EXHIBITIONS AND OUTDOOR MARKETING</p>
                            </div>

                             <div class="row">
                                <div class="col-md-9">

                                    <div class="graphic_textbox_layout_style_default">
                                      <p style="padding-bottom: 0pt; padding-top: 0pt; " class="paragraph_style_10">Stand out from the crowd with Exhibitions and Outdoor Marketing that make a real impact</p>
                                    </div>

                                    <div class="text-content graphic_textbox_layout_style_default_External_637_137" style="padding: 0px; ">
                                        <div class="graphic_textbox_layout_style_default">
                                          <p style="padding-top: 0pt; " class="paragraph_style_16">So, your online marketing channels are converting, you have a great range of printed products and a<br /></p>
                                          <p class="paragraph_style_16">social media strategy that is leaving your competitors behind – but what about exhibitions and outdoor<br /></p>
                                          <p class="paragraph_style_16">marketing? Are you maximizing your brand message when it comes to making a sizable impact?<br /></p>
                                          <p class="paragraph_style_16">Our expert designers have worked with both major national organizations and small local companies to<br /></p>
                                          <p class="paragraph_style_16">provide every kind of promotional product, from full exhibition stands and outdoor marketing materials<br /></p>
                                          <p style="padding-bottom: 0pt; " class="paragraph_style_16">to point of sale materials and branded gifts.</p>
                                        </div>
                                      </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div id="carousel-example-generic" style="height: 140px;width:198px;margin-left: -27px;" class="carousel slide" data-ride="carousel">

                                        <div class="carousel-inner" style="height: 100%;" >

                                          <div class="item active" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../uploads/ads/ojok.png" alt="islemedia">

                                          </div>
                                          <div class="item" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../uploads/ads/1.jpg" alt="islemedia">

                                          </div>
                                          <div class="item" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../uploads/ads/2.jpg" alt="islemedia">

                                          </div>
                                          <div class="item" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../uploads/ads/3.jpg" alt="islemedia">

                                          </div>


                                        </div>
                                        <!-- Controls -->
                                        <ol class="carousel-indicators" style="height:70%;padding-top:250px;">
                                          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                                        </ol>
                                    </div>
                                  </div>

                              </div>


                              <div class="graphic_textbox_layout_style_default">
                                  <p style="padding-bottom: 0pt; padding-top: 0pt; " class="paragraph_style_11">Engage with Key Business Prospects</p>
                                </div>


                                <div class="text-content graphic_textbox_layout_style_default_External_637_229" style="padding: 0px; ">

                                <div class="row">
                                    <div class="col-md-9">

                                        <div class="graphic_textbox_layout_style_default">
                                          <p style="padding-top: 0pt; " class="paragraph_style_17">For many businesses, exhibition and events marketing offers a valuable opportunity to engage with key<br /></p>
                                          <p class="paragraph_style_17">business prospects in real terms. Customers, investors, industry peers and suppliers may all be at your<br /></p>
                                          <p class="paragraph_style_17">next event so it is crucial that you consider your promotional material and what it can achieve for your<br /></p>
                                          <p class="paragraph_style_17">brand.<br /></p>
                                          <p class="paragraph_style_17">Attending an event or exhibition involves thinking about and clarifying your objectives prior to<br /></p>
                                          <p class="paragraph_style_17">attendance. Here at <span class="style_1">Isle Media Corporations Ltd</span> we can work with you to ensure that your material reflects your<br /></p>
                                          <p class="paragraph_style_17">objectives. For example if you would like to carry out market research at the event or demonstrate a<br /></p>
                                          <p class="paragraph_style_17">product we can work with you to create a full stand or accessories focused on this goal.<br /></p>
                                          <p class="paragraph_style_17">Our graphic design team can work in a way to suit you. We can send full print-ready artwork to your<br /></p>
                                          <p class="paragraph_style_17">regular supplier if you have one or work with our own network of manufacturers to provide high-quality,<br /></p>
                                          <p class="paragraph_style_17">competitively priced exhibition and promotional products. Using our experience and trusted contacts in<br /></p>
                                          <p class="paragraph_style_17">the exhibition trade we can offer competitive prices on materials and can handle all the production and<br /></p>
                                          <p style="padding-bottom: 0pt; " class="paragraph_style_17">delivery requirements efficiently and within a deadline.</p>
                                        </div>
                                    </div>                                   
                                    <div class="col-md-3">
                                        <div id="carousel-example-generic" style="height: 140px;width:198px;margin-left: -27px;" class="carousel slide" data-ride="carousel">

                                        <div class="carousel-inner" style="height: 100%;" >

                                          <div class="item active" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../uploads/ads/ojok.png" alt="islemedia">

                                          </div>
                                          <div class="item" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../uploads/ads/1.jpg" alt="islemedia">

                                          </div>
                                          <div class="item" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../uploads/ads/2.jpg" alt="islemedia">

                                          </div>
                                          <div class="item" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../uploads/ads/3.jpg" alt="islemedia">

                                          </div>


                                        </div>
                                        <!-- Controls -->
                                        <ol class="carousel-indicators" style="height:70%;padding-top:250px;">
                                          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                                        </ol>
                                    </div>
                                    </div>
                                </div>
                              </div>

                              <div class="graphic_textbox_layout_style_default">
                                  <p style="padding-bottom: 0pt; padding-top: 0pt; " class="paragraph_style_11">Outdoor Marketing</p>
                                </div>


                            <div class="text-content graphic_textbox_layout_style_default_External_637_195" style="padding: 0px; ">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="graphic_textbox_layout_style_default">
                                      <p style="padding-top: 0pt; " class="paragraph_style_18">Outdoor marketing offers you the chance to display your brand on a large scale if you opt for channels<br /></p>
                                      <p class="paragraph_style_18">such as billboard advertising or transport advertising. Or perhaps you would like to look at smaller<br /></p>
                                      <p class="paragraph_style_18">creative solutions which are strategically placed in high footfall areas such as shopping centers,<br /></p>
                                      <p class="paragraph_style_18">entertainment venues and more.<br /></p>
                                      <p class="paragraph_style_18">Additionally, you can take advantage of specifically targeted location based channels. Billboard sites or<br /></p>
                                      <p class="paragraph_style_18">transport routes might be an option for your business if you are looking to engage key customers in<br /></p>
                                      <p class="paragraph_style_18">order to promote a new store opening or upcoming event.<br /></p>
                                      <p class="paragraph_style_18">Our Marketing team can offer advice or plan a full campaign across multiple channels or if you prefer,<br /></p>
                                      <p class="paragraph_style_18">we can simply work with you to create and manage your artwork production across a network of<br /></p>
                                      <p class="paragraph_style_18">suppliers.<br /></p>
                                      <p style="padding-bottom: 0pt; " class="paragraph_style_18">Contact us today to find out more about what we can do to create an impact for your business.</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div id="carousel-example-generic" style="height: 140px;width:198px;margin-left: -27px;" class="carousel slide" data-ride="carousel">

                                        <div class="carousel-inner" style="height: 100%;" >

                                          <div class="item active" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../uploads/ads/ojok.png" alt="islemedia">

                                          </div>
                                          <div class="item" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../uploads/ads/1.jpg" alt="islemedia">

                                          </div>
                                          <div class="item" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../uploads/ads/2.jpg" alt="islemedia">

                                          </div>
                                          <div class="item" style="height: 100%;">
                                            <img style="height:230px;width: 100%;" src="../../uploads/ads/3.jpg" alt="islemedia">

                                          </div>


                                        </div>
                                        <!-- Controls -->
                                        <ol class="carousel-indicators" style="height:70%;padding-top:250px;">
                                          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                          </div>
                                  

                    </div>
                </div>
            </div>

            <div class="row" style="">
                <div class="col-md-8">
                   <p class="white paragraph_style_2">2016 Copyrighted. Produced & Designed by:<br>Isle Media Corporations Ltd.</p>
                </div>
                <div class="col-md-4">
                   <p class="white paragraph_style_2" style="text-align:right;">Contact:<br> tel:  &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +256 75 206 4714 <br>+256 77 907 6542<br>email:  <a title="mailto:islemedia@gmail.com" class="white" href="mailto:islemedia@gmail.com"><u>islemedia@gmail.com</u></a><br>info.islemediacorp.com

                   </p>
                </div>
            </div>

            </div><!-- End of Main page section-->

	        <script src="../../externals/bootstrap-3.2.0-dist/js/bootstrap.min.js"></script> 
            
	        
    </body>
</html>